package com.android.szkolenie.szkoleniew3_fragmenty;

import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by papcio28 on 09.05.2015.
 */
public class ListaFragment extends Fragment {

    private ListView mListView;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Do zmiennej container nie mozna dodawac podwidokow
        // container jest dla inflatera do wymiarowania

        View mView = inflater.inflate(R.layout.fragment_lista, container, false);
        // false na końcu, aby nowo tworzony widok nie został od razu umieszczony w container

        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        // Zmienna view to jest to samo co utworzylismy w metodzie onCreateView
        super.onViewCreated(view, savedInstanceState);  // Nie usuwać tej linijki

        mListView = (ListView) view.findViewById(R.id.lista);

        ArrayAdapter<String> mAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, getLista());

        mListView.setAdapter(mAdapter);
    }

    public String[] getLista() {
        return new String[] { "Warszawa", "Wrocław", "Poznań", "Katowice", "Gdańsk" };
    }
}
