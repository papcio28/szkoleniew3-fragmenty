package com.android.szkolenie.szkoleniew3_fragmenty;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by papcio28 on 09.05.2015.
 */
public class SzczegolyFragment extends Fragment {

    private TextView mMiastoNazwa;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_szczegoly, container, false);

        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mMiastoNazwa = (TextView) view.findViewById(R.id.miasto_nazwa);
    }
}
